package main

import (
	"github.com/go-gl/mathgl/mgl32"
	"github.com/go-gl/gl/v4.1-compatibility/gl"
	"strings"
	"fmt"
)

func loadShaders() {
	// Configure the vertex and fragment shaders
	program, err := newProgram(vertexShader, fragmentShader)
	if err != nil {
		panic(err)
	}

	gl.UseProgram(program)

	location_transformation = gl.GetUniformLocation(program, gl.Str("transformationMatrix\x00"))
	location_projection = gl.GetUniformLocation(program, gl.Str("projectionMatrix\x00"))
	location_view = gl.GetUniformLocation(program, gl.Str("viewMatrix\x00"))

	projection = createProjectionMatrix()
	loadMatrix(location_projection, &projection)//Ep. 8 11:11

}

//ShaderProgram
func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	//MIGHT NEED TO BINDATTRIBUTES HERE INSTEAD OF ELSE WHERE
	bindAllAttributes()
	gl.LinkProgram(program)
	gl.ValidateProgram(program)
	//getAllUniformLocations(program)
	/**var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}*/

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func bindAllAttributes() {
	gl.BindAttribLocation(program, 0, gl.Str("position\x00"))
	gl.BindAttribLocation(program, 1, gl.Str("textureCoords\x00"))
}

/**func loadTransformationMatrix(matrix *mgl32.Mat4) {
	loadMatrix(projectionUniform, matrix)
}*/
/**var tranformationLocation int32
func getAllUniformLocations(program uint32) {
	//Ep. 7 8:48
	location_transformation = gl.GetUniformLocation(program, gl.Str("transformationMatrix\x00"))
	location_projection = gl.GetUniformLocation(program, gl.Str("projectionMatrix\x00"))
	location_view = gl.GetUniformLocation(program, gl.Str("viewMatrix\x00"))
}*/

/**func getUniformLocation(program uint32, uniformName string) int32 {
	return gl.GetUniformLocation(program, gl.Str(uniformName))
}*/

/*func bindAttributes(program, attribute uint32, variableName string) {
	gl.BindAttribLocation(program, attribute, gl.Str(variableName))
}*/

func loadFloat(location int32, value float32) {
	gl.Uniform1f(location, value)
}

func loadVector(location int32, vector mgl32.Vec3) {
	gl.Uniform3f(location, vector.X(), vector.Y(), vector.Z())
}

func loadBoolean(location int32, value bool) {
	var toLoad float32 = 0
	if value {
		toLoad = 1
	}
	gl.Uniform1f(location, toLoad)
}

func loadMatrix(location int32, matrix *mgl32.Mat4) {
	gl.UniformMatrix4fv(location,1,false, &matrix[0])
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}
