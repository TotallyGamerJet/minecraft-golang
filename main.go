package main

import (
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/gl/v4.1-compatibility/gl"
	"runtime"
	"log"
	"fmt"
	_"image/png"
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

func move(w *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	currentSpeed = 0
	currentStrafeSpeed = 0
	if w.GetKey(glfw.KeyW) == glfw.Press {
		currentSpeed = runSpeed
		distance := currentSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(dx), 0, float32(-dz)})
	}
	if w.GetKey(glfw.KeyS) == glfw.Press {
		currentSpeed = runSpeed
		distance := currentSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(-dx), 0, float32(dz)})
	}
	if w.GetKey(glfw.KeyA) == glfw.Press {
		currentStrafeSpeed = runSpeed
		distance := currentStrafeSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw + 90)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw + 90)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(-dx), 0, float32(dz)})
	}
	if w.GetKey(glfw.KeyD) == glfw.Press {
		currentStrafeSpeed = runSpeed
		distance := currentStrafeSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw + 90)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw + 90)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(dx), 0, float32(-dz)})
	}

	if w.GetKey(glfw.KeySpace) == glfw.Press  {
		camera.position = camera.position.Add(mgl32.Vec3{0, 0.02, 0})
	}
	if w.GetKey(glfw.KeyLeftShift) == glfw.Press  {
		camera.position = camera.position.Add(mgl32.Vec3{0, -0.02, 0})
	}
	if w.GetKey(glfw.KeyEscape) == glfw.Press {
		w.SetInputMode(glfw.CursorMode, glfw.CursorNormal)
	}
}

func mouse(w *glfw.Window, xpos, ypos float64) {
//	var lastTime float64
	//currentTime := glfw.GetTime()
	//deltaTime := float32(currentTime - lastTime)
//	lastTime = currentTime

	//moves camera
	camera.yaw = -turnSpeed * float32(delta) * float32(width/2 - xpos)
	camera.pitch = -turnSpeed * float32(delta) * float32(height/2 - ypos)

	if camera.pitch > 90 {
		camera.pitch = 90
	} else if camera.pitch < -90 {
		camera.pitch = -90
	}
}

func main() {

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	window, _ := createDisplay()

	defer closeDisplay(window)

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	loadShaders()
	defer gl.UseProgram(0)

	model := createModel(vertices, textureCoords, indices, "stone.png")
	blockList := fill(model, 0, 0, 0, 16, 16, 16)
	camera = Camera{mgl32.Vec3{0, 1.5, 0}, 0, 0}

	window.SetKeyCallback(move)
	window.SetSizeCallback(onResize)
	window.SetCursorPosCallback(mouse)
	window.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)

	for !window.ShouldClose() {
		prepare()

		for _, y := range blockList {
			for _, x := range y {
				for _, block := range x {
					processEntity(block.renderable)
				}
			}
		}

		renderEntities(entities)
		glfw.SwapInterval(1)
		updateDisplay()
		// Maintenance
		window.SwapBuffers()
	}
}