package main

import (
	"math"
	"github.com/go-gl/mathgl/mgl32"
)

//Maths
func degreesToRadians(v float32) float32 {
	return v * (math.Pi / 180)
}

func radiansToDegrees(v float32) float32 {
	return v * (180 / math.Pi)
}

func createTransformationMatrix(translation mgl32.Vec3, rx, ry, rz, scale float32) mgl32.Mat4 {
	matrix := mgl32.Ident4()
	matrix = matrix.Mul4(mgl32.Translate3D(translation.X(), translation.Y(), translation.Z()))
	matrix = matrix.Mul4(mgl32.HomogRotate3DX(rx))
	matrix = matrix.Mul4(mgl32.HomogRotate3DY(ry))
	matrix = matrix.Mul4(mgl32.HomogRotate3DZ(rz))
	matrix = matrix.Mul4(mgl32.Scale3D(scale, scale, scale))
	return matrix
}

func createViewMatrix(c Camera) mgl32.Mat4 {
	matrix := mgl32.Ident4()
/*	matrix = mgl32.LookAtV(
		c.position,
		mgl32.Vec3{float32(math.Cos(float64(degreesToRadians(c.pitch))) * math.Sin(float64(degreesToRadians(c.yaw)))),
			float32(math.Sin(float64(degreesToRadians(c.pitch)))),
			float32(math.Cos(float64(degreesToRadians(c.pitch))) * math.Cos(float64(degreesToRadians(c.yaw))))}.Add(c.position),
		mgl32.Vec3{0,1,0},
	)*/
	matrix = matrix.Mul4(mgl32.HomogRotate3DX(degreesToRadians(c.pitch)))
	matrix = matrix.Mul4(mgl32.HomogRotate3DY(degreesToRadians(c.yaw)))
	matrix = matrix.Mul4(mgl32.Translate3D(-c.position.X(), -c.position.Y(), -c.position.Z()))
	return matrix
}
