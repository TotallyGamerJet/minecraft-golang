package main

import (
	"github.com/go-gl/mathgl/mgl32"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/gl/v4.1-compatibility/gl"
)

//RENDERER
func prepare() {
	gl.Enable(gl.DEPTH_TEST)
	//gl.Enable(gl.CULL_FACE)

	gl.ClearColor(1.0, 0, 0, 1.0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	glfw.PollEvents()
	viewMatrix := createViewMatrix(camera)//THIS IS PROBABLY WRONG
	loadMatrix(location_view, &viewMatrix)
	entities = make(map[TexturedModel][]Entity)

}

func renderEntities(renderables map[TexturedModel][]Entity) {
	for texturedmodel := range renderables {
		prepareTexturedModel(texturedmodel)
		batch := renderables[texturedmodel]
		for _, entity := range batch {
			prepareInstance(entity)
			gl.DrawElements(gl.TRIANGLES, entity.texturedmodel.rawModel.vertexCount, gl.UNSIGNED_INT, gl.PtrOffset(0))
		}
		unbindTexturedModel()
	}
	entities = nil
}

func processEntity(renderable Entity) {
	model := renderable.texturedmodel
	batch := entities[model]
	if batch != nil {
		batch = append(batch, renderable)
		entities[model] = batch
	} else {
		var batch = make([]Entity, 16)
		batch[0] = renderable
		entities[model] = batch
	}
}

func prepareTexturedModel(texture TexturedModel) {
	model := texture.rawModel
	gl.BindVertexArray(model.vaoID)
	//vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(0)
	gl.EnableVertexAttribArray(1)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture.textureID)
}

func unbindTexturedModel() {
	gl.DisableVertexAttribArray(0)
	gl.DisableVertexAttribArray(1)
	gl.BindVertexArray(0)
}

func prepareInstance(renderable Entity) {
	//ADDED IN AT EP. 8 3:32
	transformationMatrix := createTransformationMatrix(renderable.position, renderable.rotX, renderable.rotY, renderable.rotZ, renderable.scale)
	loadMatrix(location_transformation, &transformationMatrix)
}

func render(renderable Entity) {
	texturedmodel := renderable.texturedmodel
	model := renderable.texturedmodel.rawModel
	gl.BindVertexArray(model.vaoID)
	//vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(0)
	gl.EnableVertexAttribArray(1)

	transformationMatrix := createTransformationMatrix(renderable.position, renderable.rotX, renderable.rotY, renderable.rotZ, renderable.scale)
	loadMatrix(location_transformation, &transformationMatrix)
	viewMatrix := createViewMatrix(camera)
	loadMatrix(location_view, &viewMatrix)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texturedmodel.textureID)
	//gl.DrawArrays(gl.TRIANGLES, 0, model.vertexCount)
	gl.DrawElements(gl.TRIANGLES, model.vertexCount, gl.UNSIGNED_INT, gl.PtrOffset(0))
	gl.DisableVertexAttribArray(0)
	gl.DisableVertexAttribArray(1)
	gl.BindVertexArray(0)
}

func onResize(w *glfw.Window, width, height int) {
	aspectRatio := float32(width)/float32(height)
	projection = mgl32.Perspective(mgl32.DegToRad(fov), aspectRatio, nearPlane, farPlane)
	loadMatrix(location_projection, &projection)

	gl.Viewport(0, 0, int32(width), int32(height))
}

func createProjectionMatrix() mgl32.Mat4{
	aspectRatio := float32(width)/height
	return mgl32.Perspective(mgl32.DegToRad(fov), aspectRatio, nearPlane, farPlane)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

