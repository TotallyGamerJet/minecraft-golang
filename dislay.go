package main

import (
	"fmt"
	"github.com/go-gl/glfw/v3.2/glfw"
)
//DISPLAY

func createDisplay() (*glfw.Window, error) {
	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 2)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(width, height, title, nil, nil)
	check(err)
	window.MakeContextCurrent()
	lastFrameTime = glfw.GetTime()
	return window, err
}

func updateDisplay() {
	currentFrameTime := glfw.GetTime()
	delta = currentFrameTime - lastFrameTime //might need to change from miliseconds to seconds by dividing by 1000
	lastFrameTime = currentFrameTime
	FPSCounter()
}

var nbFrames = 0
var lastTime float64 = 0
func FPSCounter() {
	currentTime := glfw.GetTime()
	nbFrames++
	if currentTime - lastTime >= 1.0 {
		fmt.Printf("%f ms/frame\n", 1000.0/float64(nbFrames))
		nbFrames = 0
		lastTime += 1.0
	}
}

func closeDisplay(window *glfw.Window) {
	window.Destroy()
}
