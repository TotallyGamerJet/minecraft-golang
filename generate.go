package main

import "github.com/go-gl/mathgl/mgl32"

func fill(model TexturedModel, x1 , y1, z1, x2 , y2, z2 int) [16][16][16]Block {
	var blockList [16][16][16]Block
	for y := y1; y < y2; y++ {
		for x := x1; x < x2; x++ {
			for z := z1; z < z2; z++ {
				block := Block{
					Entity{model, mgl32.Vec3{float32(x) * float32(0.5), float32(z) * float32(0.5), float32(y) * float32(-0.5)}, 0, 0, 0, 0.5},
				}
				blockList[x][y][z] = block
			}
		}
	}
	return blockList
}
