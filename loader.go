package main

import (
	"os"
	"fmt"
	"image"
	"image/draw"
	"github.com/go-gl/gl/v4.1-compatibility/gl"
)

//LOADER
func createModel(positions, textureCoords []float32, indices []int32, texture string) TexturedModel {
	model := loadToVAO(positions,textureCoords, indices)
	textureID, err := loadTexture(texture)
	check(err)
	texturedModel := TexturedModel{model, textureID}
	return texturedModel
}

func loadToVAO(positions, textureCoords []float32, indices []int32) RawModel {
	vaoID := createVAO()
	bindIndicesBuffer(indices)
	storeDataInAttributeLists(0, 3, positions)
	storeDataInAttributeLists(1, 2, textureCoords)
	unbindVAO()
	return RawModel{vaoID, int32(len(indices))} //THIS int32(...) might be wrong!
}

func loadTexture(file string) (uint32, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return 0, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return 0, err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))

	gl.GenerateMipmap(gl.TEXTURE_2D) //ADDS IN MIPMAPPING TO SPEED UP GAME
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR)
	gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_LOD_BIAS, -0.5)

	return texture, nil
}

func createVAO() uint32 {
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	//SHOULD I ADD vao TO A SLICE FOR MEMORY MANAGEMENT? -EP 2 11:25
	gl.BindVertexArray(vao)
	return vao
}

func storeDataInAttributeLists(attrib uint32, coordSize int32, data []float32) {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	//SHOULD I ADD vbo TO A SLICE FOR MEMORY MANAGEMENT? -EP 2 11:25
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(data)*4, gl.Ptr(data), gl.STATIC_DRAW)

	gl.VertexAttribPointer(attrib, coordSize, gl.FLOAT, false, 0/**(5*4)*/, gl.PtrOffset(0))

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
}

func unbindVAO() {
	gl.BindVertexArray(0)
}

func bindIndicesBuffer(indices []int32) {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	//SHOULD I ADD vbo TO A SLICE FOR MEMORY MANAGEMENT? -EP 3 7:00
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indices)*4, gl.Ptr(indices), gl.STATIC_DRAW)
}
